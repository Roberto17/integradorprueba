<?php



require_once 'model/aulas.php';

class AulasController{
    
    private $model;
    
    public function __construct(){
        $this->model = new Aulas();
    }    
    public function Index(){
        require_once 'view/aulas.php';
       
    }    
    public function Crud(){
        $data = new Aulas();
        
        if(isset($_REQUEST['N_aula'])){
            $data = $this->model->getByID($_REQUEST['N_aula']);
        }       
        require_once 'view/Aulas-editar.php';          
    }  
    public function add(){
        $data = new Aulas();
        
        $data->N_aula = $_REQUEST['N_aula'];
        $data->latitud = $_REQUEST['latitud'];
        $data->longitud = $_REQUEST['longitud'];
		$data->detalle = $_REQUEST['detalle'];

        $data->N_aula > 0 
            ? $this->model->update($data)
            : $this->model->add($data);        
        header('Location: indexAulas.php');
    }
    
   
}