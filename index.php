<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Proyecto integrador</title>

    <!-- Bootstrap core CSS -->
    <link href="Assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="Assets/css/small-business.css" rel="stylesheet">

  </head>

  <body>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="#">Universidad Gerardo Barrios</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
              <a class="nav-link" href="index.php">Home
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="administracion.php">Administracion</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Services</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Contact</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <!-- Page Content -->
    <div class="container">

      <!-- Heading Row 
      
          <img class="img-fluid rounded" src="http://placehold.it/900x400" alt="">
        </div>-->
		
	<div class="row my-4">
        <div class="col-lg-8">
      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
          <!-- Slide One - Set the background image for this slide in the line below -->
          <div class="carousel-item active">
		  <img src="Assets/img/1.jpg" border="0" width="700" height="400"/>
            <div class="carousel-caption d-none d-md-block">
            
            </div>
          </div>
          <!-- Slide Two - Set the background image for this slide in the line below -->
          <div class="carousel-item" >
		  <img src="Assets/img/2.jpg" border="0" width="700" height="400"/>
            <div class="carousel-caption d-none d-md-block">
          
            </div>
          </div>
		   <!-- Slide Two - Set the background image for this slide in the line below -->
          <div class="carousel-item" >
		  <img src="Assets/img/3.gif" border="0" width="700" height="400"/>
            <div class="carousel-caption d-none d-md-block">
          
            </div>
          </div>
          <!-- Slide Three - Set the background image for this slide in the line below -->
         
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
	  </div>
   
        <!-- /.col-lg-8 -->
        <div class="col-lg-4">
          <h1>geolocalizacion de La universidad Gerardo Barrios:</h1>
          <p>Este proyecto consiste en una aplicación que permita ubicar de manera más rápida a los estudiantes de nuevo ingreso permitiéndoles visualizar de forma 3d y 2d todo el croquis de la universidad. Esta aplicación les permitirá explorar de forma interactiva la universidad permitiéndoles ver donde están ubicados todas áreas de la Universidad</p>
          <a class="btn btn-primary btn-lg" href="#">Mas info!</a>
        </div>
        <!-- /.col-md-4 -->
      </div>
      <!-- /.row -->

      <!-- Call to Action Well -->
      <div class="card text-white bg-secondary my-4 text-center">
        <div class="card-body">
          <p class="text-white m-0">Menu de opciones</p>
        </div>
      </div>

      <!-- Content Row -->
      <div class="row">
        <div class="col-md-4 mb-4">
          <div class="card h-100">
            <div class="card-body">
              <h2 class="card-title">Vista 2D</h2>
              <p class="card-text">Permite al usuario ver el croquis de manera 2d ademas de poder hacer busquedas especializadas mostrando al usuario como llegar a su lugar de destino</p>
            		   <center><img src="Assets/img/botonM.jpg" border="1" width="200" height="150"/> </center>
			</div>
            <div class="card-footer">
              <a href="#" class="btn btn-primary">Entrar</a>
            </div>
          </div>
        </div>
        <!-- /.col-md-4 -->
        <div class="col-md-4 mb-4">
          <div class="card h-100">
            <div class="card-body">
              <h2 class="card-title">Vista 3D</h2>
              <p class="card-text">permite al usuario ver el croquis de manera 3d ademas de poder manipular el modelo a 360° permitiendo que el usuario conosca mejor el entorno y de una manera mas interactiva.</p>
            		   <center><img src="Assets/img/boton3d.jpg"   border="1" width="200" height="150"/></center>
			</div>
            <div class="card-footer">
              <a href="#" class="btn btn-primary">Entrar</a>
            </div>
          </div>
        </div>
        <!-- /.col-md-4 -->
        <div class="col-md-4 mb-4">
          <div class="card h-100">
            <div class="card-body">
              <h2 class="card-title">Vista 2D-(GPS)</h2>
              <p class="card-text">Permite ver el croquis de manera 2d pero ademas de eso el usuario podra navegar y llegar a su lugar de destino mediante señas via GPS</p>
	   <center><img src="Assets/img/boton2gps.png"   border="1" width="200" height="150"/></center>           
		   </div>
            <div class="card-footer">
              <a href="#" class="btn btn-primary">Entrar</a>
            </div>
          </div>
        </div>
        <!-- /.col-md-4 -->

      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->

    <!-- Footer -->
    <footer class="py-5 bg-dark">
      <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Your Website 2017</p>
      </div>
      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="Assets/vendor/jquery/jquery.min.js"></script>
    <script src="Assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  </body>

</html>
